---
layout: post
title:  "Kotlin Coroutines suspend function在Dynamic Proxy的處理"
date:   2021-11-22 00:00:00 +0800
categories: chinese programming kotlin
tags: kotlin coroutines reflection dynamic_proxy
comments: true
---
因為工作需求，自己用Kotlin寫了一個類似於Python Decarator的little framework，原理只要有了解過Java EE的都會知道，就是用Dynamic Proxy把function invocation變成可控行為，但這點在面對suspend function時就出了問題，上code再解釋

```kotlin
@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class TestAnnotation

class Test {
    @TestAnnotation
    suspend fun test() = true
}

val invocationHandler = object: InvocationHandler {
    override fun invoke(proxy: Any?, method: Method?, args: Array<out Any>?): Any? {
        return args?.also { method.invoke(obj, *it) } ?: method.invoke(obj)
    }
}

val proxy = Proxy.newProxyInstance(
    Test::class.java.classloader,
    Text::class.java.interfaces,
    invocationHandler
) as Test

CoroutineScope(Dispatchers.IO).launch {
    val result = proxy.test()
    // result = ??
}
```

上面那段code的result會是甚麼?~~是Jojo噠!~~
答案是`CoroutineSingletons.COROUTINE_SUSPENDED`, 相關部份在[這裡](https://github.com/JetBrains/kotlin/blob/master/libraries/stdlib/src/kotlin/coroutines/intrinsics/Intrinsics.kt)

如果你有把那個`invocationHandler#invoke`的args print出來看，你會發現那不是null，而是長度為1的array，那個item是[Continuation](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.coroutines/-continuation/)，這是因為Kotlin的Coroutines的實現原理是CPS變換，Continuation正是其結果，用正常的方式invoke只能得到一個等待執行的Continuation

那有甚麼解決方法呢？利用Kotlin reflect lib就行
```kotlin
// invocationHanlder#invoke
override fun invoke(proxy: Any?, method: Method?, args: Array<out Any>?): Any? {
    args?.also { method.kotlinFunction!!.callSuspend(obj, *it.filter { it !is Continuation<*> }.toTypedArray()) } ?: method.kotlinFunction!!.callSuspend(obj)
}
```

那該怎麼判斷是不是suspend function呢?有幾個方法
1. 從method args type找有沒有Continuation
2. 從method signature找最後的parameter type是不是Continuation
3. 因為[callSuspend](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.reflect.full/call-suspend.html)會判斷傳進來的是不是suspend function，不是的會fallback用call，所以就直接callSuspend下去...

這樣寫好後就能不管是寫function/method都能直接@下去，寫成AOP