---
layout: post
title:  "拖延症發作"
date:   2021-06-06 23:50:00 +0800
categories: chinese other
tags: bullshit daily
comments: true
---
本來今天應該出第一篇Android databinding/viewbinding的分析文，但因為~~WTM 3D b-day~~拖延症，應該順延到下次，這兩天會寫的換成自己寫的OpenGL筆記

順便測試一下coding fence...
```cpp
#include <iostream>
#include "GLFW/glfw3.h"

int main()
{
    for (auto n : "iamgood") std::cout << n;
    std::cout << "testing";
    return 0
}
```
```glsl
#version 330 core
layout (location=0) in vec3 aPos;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
    gl_Position = projection * view * model * vec4(aPos, 1.0);
}
```