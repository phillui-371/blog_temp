---
layout: post
title:  "Mac M1 cmake config to get arm binary"
date:   2021-06-06 23:50:00 +0800
categories: english programming toolchain
tags: cmake silicon mac osx
comments: true
---
Just a note to let cmake know you want a arm bin instead of x86_64 binary. I need to make this due to my `uname -m` result is `x86_64`...

```cmake
if(${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
  # arm detection
  execute_process(
    COMMAND sysctl hw.optional.arm64
    OUTPUT_VARIABLE ARM_FLAG
  )
  if(ARM_FLAG MATCHES "hw.optional.arm64: 1")
    set(CMAKE_SYSTEM_PROCESSOR "arm64")
    set(CMAKE_OSX_ARCHITECTURES "arm64")
  endif()

  # non arch-depend stuff

  # arch-depend stuff
  if(${CMAKE_SYSTEM_PROCESSOR} MATCHES "arm64")
    # do your stuff on arm
  else(${CMAKE_SYSTEM_PROCESSOR} MATCHES "arm64")
    # do your stuff on x86
  endif(${CMAKE_SYSTEM_PROCESSOR} MATCHES "arm64")
endif(${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
```
