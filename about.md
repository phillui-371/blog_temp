---
title: "About"
layout: about
---

# Phil Lui

## Description
- full stack developer, learning to be a game developer
- Familiar with Kotlin and Python
- Learning OpenGL/C++/Rust/Haskell
- ACGMN panic

## Contact
- email: phillui37@gmail.com
- twitter: @phil_kgy
- gitlab: @phillui-371